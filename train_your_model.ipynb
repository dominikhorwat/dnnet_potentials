{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Train DNN model and determine EMO parameters for selected dimer\n",
    "-----\n",
    "\n",
    "## Methodology\n",
    "\n",
    "\n",
    "A method which employs a deep neural network (DNN) for obtaining parameters of analytical potential energy curve (PEC) of the selected electronic state of a diatomic molecule based on the energies of recorded ro-vibrational transitions. PEC is in form of expanded Morse oscillator (EMO) function with 6 parameters (EMO3), i.e:\n",
    "- De (the potential well depth),\n",
    "- Re (the internuclear equilibrium distance),\n",
    "- β0, β1, β2, β3.\n",
    "\n",
    "\n",
    "## General information\n",
    "If you want to determine parameters of EMO3 potential for any dimer you choose, you can train our model. To train the model by scratch, you have to prepare train and test dataset for your selected molecule (see **Data preparation** section). Once you have your data, all you have to do is provide the appropriate path (**Define paths** section) to your train and test data. This script also allows you to evaluate the performance of your model.\n",
    "\n",
    "Once you have trained your model, the following files will be saved on your local machine:\n",
    "\n",
    "- your trained DNN model (``my_model.h5`` by default),\n",
    "- used hyperparameters (``my_model_hyperparams.json`` by default),\n",
    "- data preprocessing models:\n",
    "  - standarizer for input data (``my_standarizer.pk`` by default),\n",
    "  - scaler for input data (``my_scaler_x.pk`` by default),\n",
    "  - scaler for output data (``my_scaler_y.pk`` by default).\n",
    "\n",
    "Once you have trained your model, you can use ``dnn_model_predictions`` notebook to determine the potential parameters for the molecule you choose.\n",
    "\n",
    "**Note:** Remember to use the preprocessing models (data preprocessors) appropriate for your model, i.e. the preprocessing models created while training your model. Data preprocessors are specific to the data used to train DNN and should only be used with that data.\n",
    "\n",
    "## Paths\n",
    "\n",
    "Depending on what data/model you are using, you should change the following paths accordingly (**Define paths** section):\n",
    "1) data and models provided by authors for the ground state in MgF.\n",
    "You should stay with default sets.\n",
    "\n",
    "2) user data for the ground state in MgF\n",
    "\n",
    "a) data paths\n",
    "- path to your train data: path_train_data (`./data/mgf/artificial/train/` by default)\n",
    "- filename of your input (energies) data: train_input_name (`inp.csv` by default)\n",
    "- filename of your output (potential parameters) data: train_output_name (`out.csv` by default)\n",
    "\n",
    "- path to your test data: path_test_data (`./data/mgf/artificial/test/` by default)\n",
    "- filename of your input (energies) data: test_input_name (`inp.csv` by default)\n",
    "- filename of your output (potential parameters) data: test_output_name (`out.csv` by default)\n",
    "\n",
    "b) config paths\n",
    "- path (and filename) where your standarizer will be saved: path_standarizer (`./configs/standarizer.pk` by default)\n",
    "- path (and filename) where your scaler for input data will be saved: path_scaler_x (`./configs/scaler_x.pk`)\n",
    "- path (and filename) where your scaler for output data will be saved: path_scaler_y (`./configs/scaler_y.pk` by default)\n",
    "\n",
    "c) model paths\n",
    "- path (and filename) where your model will be saved: path_model (`./models/model_mgf.h5` by default)\n",
    "- path (and filename) where your hyperparams used during your model training will be saved: path_hyperparams (`./models/model_mgf_hyperparams.json` by default)\n",
    "\n",
    "\n",
    "\n",
    "## Data preparation\n",
    "\n",
    "### Input\n",
    "\n",
    "Input data is formed by the energies of ro-vibrational transitions of a given molecule. Data must be in .csv. Energies for a single instance must be separated by semicolon. Each instance must be placed in the new row. Given set of energies (row i in the energies file) corresponds to the given set of potential parameters (row i in the potential parameters file). Please refer to subsection 3) to see examples. A single instance must contain 744 energy of assigned transitions between different ro-vibrational levels (see paper for more details). Thus, the shape of the entire input data vector will be (number of samples, 744).\n",
    "\n",
    "### Output\n",
    "\n",
    "Output data is formed by the parameters of EMO3 potential. Data must be in .csv. Potential parameters must be separated by semicolon. Each instance must be placed in the new row. Given set of potential parameters (row i in the potential parameters file) corresponds to the given set of energies (row i in the energies file). Please refer to subsection 3) to see examples. A single instance must contain value for 6 parameters, i.e. Re, De, β0, β1, β2, β3. Thus, the shape of the entire output data vector will be (number of samples, 6).\n",
    "\n",
    "### Example\n",
    "\n",
    "In ``data`` dir you can find data for MgF molecule. Dataset consists sample of 1000 instances of transitions energies and potential parameters splited into train and test subsets (900 and 100, respectively). This data can be directly use to play with the trained model or can be used to train your own model (this number of data is inssuficient to train the new model, to get the full dataset please contact authors). If you want to train your own model, dataset should have the same structure as example data, i.e.\n",
    "\n",
    "- potential parameters\n",
    "```\n",
    "    1.7661;37297.24;1.2570;0.4290;-0.1865;-0.1932\n",
    "    1.7473;37297.24;1.4954;0.1950;-0.0239;0.0925\n",
    "\n",
    "                      ...\n",
    "                      ...\n",
    "                      ...\n",
    "\n",
    "    1.7775;37297.24;1.4606;-0.1120;-0.0285;-0.1298\n",
    "```\n",
    "where each column stores value for Re, De, β0, β1, β2, β3 parameter, respectively.\n",
    "\n",
    "\n",
    "- transition energies\n",
    "```\n",
    "    608.3629;610.3959;611.4110;612.4250;613.4369;...;523.7549\n",
    "    723.2280;725.2779;726.2949;727.3060;728.3119;...;625.4170\n",
    "\n",
    "                          ...\n",
    "                          ...\n",
    "                          ...\n",
    "\n",
    "    704.6990;706.6650;707.6359;708.5990;709.5539;...;597.8220\n",
    "```\n",
    "where each column stores energy of assigned transitions between different ro-vibrational levels (see paper for more details).\n",
    "\n",
    "**Please refer to ``doc.txt`` to get full documentation.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------------\n",
    "-------------\n",
    "-------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import libs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "warnings.simplefilter(action='ignore', category=FutureWarning)\n",
    "import numpy as np\n",
    "np.set_printoptions(suppress=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import time\n",
    "import json\n",
    "import keras.backend as K\n",
    "from keras.callbacks import ReduceLROnPlateau, EarlyStopping, Callback\n",
    "from keras import optimizers\n",
    "from sklearn.preprocessing import MinMaxScaler, StandardScaler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pkg.data_utils import (compare_prediction_and_test, validate_prediction,\n",
    "                            show_best_worst_prediction, best_worst_prediction)\n",
    "from pkg.read_data import read_data_from_csv\n",
    "from pkg.keras_metrics import weighted_mse\n",
    "from pkg.config import EMO3_MgF_PARAMETERS\n",
    "from pkg.plots import plot_potential, plot_histograms\n",
    "from pkg.metrics import compute_l2_norm\n",
    "from pkg.potentials import compute_emo_potential, get_potential_energy_curve\n",
    "from pkg.model import load_deep_model, show_evaluation_metrics, dnn_model\n",
    "from pkg.preprocessing import dump_transformer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------------\n",
    "-------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define paths"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Train"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_train_data = \"data/mgf/artificial/train/\"\n",
    "train_input_name = \"inp.csv\"\n",
    "train_output_name = \"out.csv\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_test_data = \"data/mgf/artificial/test/\"\n",
    "test_input_name = \"inp.csv\"\n",
    "test_output_name = \"out.csv\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_standarizer = \"./configs/my_standarizer.pk\"\n",
    "path_scaler_x = \"./configs/my_scaler_x.pk\"\n",
    "path_scaler_y = \"./configs/my_scaler_y.pk\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_save_path = \"./models/my_model.h5\"\n",
    "hyperparams_save_path = \"./models/my_model_hyperparams.json\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------------\n",
    "-------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load hyperparams"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_hyperparams = \"models/model_mgf_hyperparams.json\"\n",
    "with open(path_hyperparams, 'r') as f:\n",
    "    hyperparams = json.load(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_train, y_train = read_data_from_csv(path_train_data, [train_input_name, train_output_name], delimiter=\";\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_test, y_test = read_data_from_csv(path_test_data, [test_input_name, test_output_name], delimiter=\";\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define hyperparams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hyperparams used"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(hyperparams)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set hyperparams"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "potential_type = hyperparams[\"potential_type\"]\n",
    "validation_split = hyperparams[\"validation_split\"]\n",
    "epochs = hyperparams[\"epochs\"]\n",
    "batch_size = hyperparams[\"batch_size\"]\n",
    "learning_rate = hyperparams[\"learning_rate\"] # not less than 0.00002\n",
    "beta_1 = hyperparams[\"beta_1\"]\n",
    "beta_2 = hyperparams[\"beta_2\"]\n",
    "shuffle = hyperparams[\"shuffle\"]\n",
    "factor = hyperparams[\"factor\"]\n",
    "min_lr = hyperparams[\"min_lr\"]\n",
    "activation_dense = hyperparams[\"activation_dense\"]\n",
    "activation_output = hyperparams[\"activation_output\"]\n",
    "WEIGHTS = list(map(lambda x: int(x.strip(\"[\").strip(\"]\")), hyperparams[\"weights\"].split(\",\")))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define input and output data size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_shape = (x_train.shape[1],)\n",
    "output_shape = y_train.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Split train set into train and validation sets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validation_split = 0.1\n",
    "split = int(x_train.shape[0] * validation_split)\n",
    "x_val = x_train[:split, :]\n",
    "x_train = x_train[split:, :]\n",
    "y_val = y_train[:split, :]\n",
    "y_train = y_train[split:, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocess data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler_x = MinMaxScaler()\n",
    "scaler_x.fit(x_train)\n",
    "x_train_scaled = scaler_x.transform(x_train)\n",
    "x_test_scaled = scaler_x.transform(x_test)\n",
    "x_val_scaled = scaler_x.transform(x_val)\n",
    "dump_transformer(path_scaler_x, scaler_x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Standarize"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "standardizer_x = StandardScaler(with_std=False)\n",
    "standardizer_x.fit(x_train_scaled)\n",
    "x_train_preproc = standardizer_x.transform(x_train_scaled)\n",
    "x_test_preproc = standardizer_x.transform(x_test_scaled)\n",
    "x_val_preproc = standardizer_x.transform(x_val_scaled)\n",
    "dump_transformer(path_standarizer, standardizer_x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler_y = MinMaxScaler()\n",
    "scaler_y.fit(y_train)\n",
    "y_train_preproc = scaler_y.transform(y_train)\n",
    "y_test_preproc = scaler_y.transform(y_test)\n",
    "y_val_preproc = scaler_y.transform(y_val)\n",
    "dump_transformer(path_scaler_y, scaler_y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load pretrained DNN model (MgF)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_model = \"models/model_mgf.h5\"\n",
    "model = load_deep_model(path_model, hyperparams, weighted_mse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load new model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = dnn_model(input_shape, output_shape, activation_output)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model summary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define callback functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = optimizers.Adam(learning_rate=learning_rate, beta_1=beta_1, beta_2=beta_2)\n",
    "lr_reducer = ReduceLROnPlateau(factor=factor,\n",
    "                               cooldown=10,\n",
    "                               monitor='val_mean_squared_error',\n",
    "                               patience=20,\n",
    "                               verbose=1,\n",
    "                               min_lr=min_lr)\n",
    "\n",
    "early_stopping = EarlyStopping(monitor=\"val_loss\",\n",
    "                               patience=40,\n",
    "                               verbose=1,\n",
    "                               mode='auto',\n",
    "                               restore_best_weights=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compile model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.compile(optimizer=optimizer,\n",
    "              loss=weighted_mse(WEIGHTS),\n",
    "              metrics=['mean_squared_error', 'mean_absolute_error'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "start_time = time.time()\n",
    "history_train = model.fit(x_train_preproc,\n",
    "                          y_train_preproc,\n",
    "                          validation_data=(x_val_preproc, y_val_preproc),\n",
    "                          epochs=epochs,\n",
    "                          batch_size=batch_size,\n",
    "                          callbacks=[lr_reducer, early_stopping],\n",
    "                          shuffle=shuffle,\n",
    "                          use_multiprocessing=True,\n",
    "                          workers=3)\n",
    "stop_time = time.time()\n",
    "print(\"Train time: {}\".format(stop_time - start_time))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_hypermarams = {\"validation_split\": validation_split,\n",
    "                   \"epochs\": epochs,\n",
    "                   \"batch_size\": batch_size,\n",
    "                   \"optimizer\": \"adam\",\n",
    "                   \"learning_rate\": learning_rate,\n",
    "                   \"beta_1\": beta_1,\n",
    "                   \"beta_2\": beta_2,\n",
    "                   \"weights\": str(WEIGHTS),\n",
    "                   \"shuffle\": shuffle,\n",
    "                   \"factor\": factor,\n",
    "                   \"min_lr\": min_lr,\n",
    "                   \"input_shape\": input_shape,\n",
    "                   \"output_shape\": output_shape,\n",
    "                   \"activation_dense\": activation_dense,\n",
    "                   \"activation_output\": activation_output,\n",
    "                   \"x_size\": \"{}\".format(x_train_preproc.shape),\n",
    "                   \"y_size\": \"{}\".format(y_train_preproc.shape),\n",
    "                   \"x_scale\": True,\n",
    "                   \"y_scale\": True,\n",
    "                   \"x_standarization\": True,\n",
    "                   \"sigma\": False,\n",
    "                   \"mean\": True,\n",
    "                   \"y_standarization\": False}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.save(model_save_path)\n",
    "with open(os.path.join(hyperparams_save_path, ), 'w') as outfile:\n",
    "    json.dump(new_hypermarams, outfile)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prediction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "start_time = time.time()\n",
    "y_pred = model.predict(x_test_preproc)\n",
    "y_pred = scaler_y.inverse_transform(y_pred)\n",
    "stop_time = time.time()\n",
    "prediction_time = round(stop_time - start_time, 6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "history_test = model.evaluate(x_test_preproc, y_test_preproc, verbose=0, workers=4, use_multiprocessing=True)\n",
    "show_evaluation_metrics(history_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate predictions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compare predictions and true data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "abs_difference, mean_difference, std_difference, _min, _max = validate_prediction(y_test, y_pred)\n",
    "compare_prediction_and_test(mean_difference, std_difference, _min, _max, EMO3_MgF_PARAMETERS)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_histograms(abs_difference, EMO3_MgF_PARAMETERS)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show the best prediction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "radii = list(np.arange(1,8,0.01))\n",
    "potential_type = \"emo3\"\n",
    "best_y_pred, best_y_test, best_diff, best_mse = best_worst_prediction(y_test, y_pred, abs_difference, np.min)\n",
    "best_l2_norm = compute_l2_norm(radii, best_y_test, best_y_pred, potential_type)\n",
    "show_best_worst_prediction(best_y_pred, best_y_test, best_diff, best_mse, best_l2_norm, potential_type, \"best\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot PEC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "radii = list(np.arange(1.12,8,0.01))\n",
    "potentials_best = get_potential_energy_curve(radii, best_y_pred, potential_type)\n",
    "potentials_best_ref = get_potential_energy_curve(radii, best_y_test, potential_type)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_potential(radii, potentials_best_ref, potentials_best)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show the worst prediction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "worst_y_pred, worst_y_test, worst_diff, worst_mse = best_worst_prediction(y_test, y_pred, abs_difference, np.max)\n",
    "worst_l2_norm = compute_l2_norm(radii, worst_y_test, worst_y_pred, potential_type)\n",
    "show_best_worst_prediction(worst_y_pred, worst_y_test, worst_diff, worst_mse, worst_l2_norm, potential_type, \"worst\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot PEC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "radii = list(np.arange(1.12,8,0.01))\n",
    "potentials_worst = get_potential_energy_curve(radii, worst_y_pred, potential_type)\n",
    "potentials_worst_ref = get_potential_energy_curve(radii, worst_y_test, potential_type)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_potential(radii, potentials_worst_ref, potentials_worst)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
