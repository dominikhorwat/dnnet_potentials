import numpy as np
import pandas as pd
from os.path import join, split


def show_data_info(x, y, datatype):
    """ Show short data summary. """
    print("Number of {} data pairs: {}".format(datatype.lower(), x.shape[0]))
    print("Input size: {}".format(x.shape[1]))
    print("Output size (potential parameters): {}".format(y.shape[1]))


def data_crosscheck(x, y):
    """ Check if size of input and output data is equal. """
    if x.shape[0] != y.shape[0]:
        raise ValueError("Input and output have different size!")
    if np.isnan(x).any():
        raise ValueError("Input data contains NaN values!")
    if np.isnan(x).any():
        raise ValueError("Output data contains NaN values!")


def strip_if_nan_or_zero(data):
    """ Remove first/last column/row if contains only zeros or NaN's. """
    if (data[:, 0] == 0).all():
        data = data[:, 1:]
    if np.isnan(data[:, -1]).all():
        data = data[:, :-1]
    if np.isnan(data[-1, :]).all():
        data = data[:, :-1]
    return data


def extract_values_from_line(line, filetype, delimiter=None):
    """ Extract data from the specific line in the .txt data file. """
    if not delimiter:
        if filetype == "input":
            line = line.split(" ")
        elif filetype == "output":
            line = line.split(";")
        else:
            line = line.split(" ")
    else:
        line = line.split(delimiter)
    line = list(map(lambda s: s.strip(";").strip("\n"), line))
    line = list(filter(None, line))
    line = list(map(lambda s: float(s), line))

    return line


def read_data(path, fnames, delimiter=None):
    """ Read train and test data. """
    print("Load data...")
    x_fname, y_fname = fnames
    x_fname = join(path, x_fname)
    y_fname = join(path, y_fname)
    datatype = split(path)[-1]

    f = open(x_fname, "r")
    x = [extract_values_from_line(line, "output", delimiter)
         for line in f.readlines()]
    f.close()
    empty_rows = [i for i, l in enumerate(x) if not l]
    x = list(filter(None, x))
    x = np.array(x)

    f = open(y_fname, "r")
    y = [extract_values_from_line(line, "input", delimiter)
         for line in f.readlines()]
    f.close()
    y = [l for i, l in enumerate(y) if i not in empty_rows]
    y = np.array(y)

    x = strip_if_nan_or_zero(x)
    y = strip_if_nan_or_zero(y)
    data_crosscheck(x, y)
    show_data_info(x, y, datatype)

    return x, y


def read_data_from_csv(path, fnames, delimiter=None):
    """ Read train and test data. """
    print("Load data...")
    x_fname, y_fname = fnames
    x_fname = join(path, x_fname)
    y_fname = join(path, y_fname)
    datatype = split(path)[-1]
    x = pd.read_csv(x_fname, header=None, delimiter=delimiter)
    y = pd.read_csv(y_fname, header=None, delimiter=delimiter)
    x = x.dropna(how="all", axis=1).dropna(how="any", axis=0).values
    y = y.dropna(how="all", axis=1).dropna(how="any", axis=0).values
    data_crosscheck(x, y)
    show_data_info(x, y, datatype)

    return x, y
    
    
def read_test_input_from_csv(path, fname, delimiter=None):
    """ Read test data. """
    print("Load test data...")
    x_fname = join(path, fname)
    x = pd.read_csv(x_fname, header=None, delimiter=delimiter)
    x = x.dropna(how="all", axis=1).dropna(how="any", axis=0).values
    if x.ndim == 1:
    	x = x.reshape([1, x.shape[0]])
    if np.isnan(x).any():
    	raise ValueError("Output data contains NaN values!")
    print("Number of test data pairs: {}".format(x.shape[0]))
    print("Input size: {}".format(x.shape[1]))
    
    return x
