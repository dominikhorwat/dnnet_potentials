from keras.models import load_model
import time
import numpy as np
np.set_printoptions(suppress=True)
from keras.models import Model
from keras.layers import Input, Dense, Activation, LeakyReLU
from keras.initializers import TruncatedNormal, VarianceScaling


def dnn_model(input_shape, output_shape, activation_output):
	inputs = Input(shape=input_shape)

	dense1 = Dense(60, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(inputs)
	act1 = LeakyReLU(alpha=0.5)(dense1)

	dense2 = Dense(40, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(act1)
	act2 = LeakyReLU(alpha=0.5)(dense2)

	dense3 = Dense(30, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(act2)
	act3 = LeakyReLU(alpha=0.5)(dense3)

	dense4 = Dense(20, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(act3)
	act4 = LeakyReLU(alpha=0.5)(dense4)

	dense5 = Dense(20, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(act4)
	act5 = LeakyReLU(alpha=0.5)(dense5)

	dense6 = Dense(20, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(act5)
	act6 = LeakyReLU(alpha=0.5)(dense6)

	dense7 = Dense(20, kernel_initializer=VarianceScaling(), bias_initializer=TruncatedNormal())(act6)
	act7 = LeakyReLU(alpha=0.5)(dense6)

	outputs = Dense(output_shape, kernel_initializer=VarianceScaling(),
		        bias_initializer=TruncatedNormal(), activation=activation_output)(act7)

	model = Model(inputs, outputs)

	return model


def load_deep_model(path_model, hyperparams, loss_function):
    WEIGHTS_DEEP = list(map(lambda s: int(s.strip("[").strip("]").strip()), hyperparams["weights"].split(",")))
    model = load_model(path_model, custom_objects={"custom_mean_squared_error": loss_function(WEIGHTS_DEEP)})
    
    return model


def make_predictions(x_test, scaler_y, model):
    if x_test.ndim == 1:
    	x_test = x_test.reshape([1, x_test.shape[0]])
    start_time = time.time()
    y_pred = model.predict(x_test)
    y_pred = scaler_y.inverse_transform(y_pred)
    stop_time = time.time()
    print("Predition time: {}".format(round(stop_time - start_time, 6)))
    return y_pred.round(5)


def evaluate_model(model, x_test, y_test):
    history_test = model.evaluate(x_test, y_test, verbose=0, workers=4, use_multiprocessing=True)
    show_evaluation_metrics(history_test)


def show_evaluation_metrics(history_test):
    print("| Loss     |    {:9f} |".format(history_test[0]))
    print("|----------|--------------|")
    print("| MSE      |    {:9f} |".format(history_test[1]))
    print("|----------|--------------|")
    print("| AMSE     |    {:9f} |".format(history_test[2]))
    print("|----------|--------------|")
    print("")
    print("MSE - Mean Squered Error")
    print("AMSE - Absolute MSE")


def show_predictions(y_pred):
    print("| Re         | De             | Beta0      | Beta1      | Beta2      | Beta3      |")
    print("|------------|----------------|------------|------------|------------|------------|")
    for y in y_pred:
        s = "|"
        for p in y:
            if p < 0:
                s += " {:4f}  |"
            else:
                s += " {:4f}   |"
        print(s.format(y[0], y[1], y[2], y[3], y[4], y[5]))
