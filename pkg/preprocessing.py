import pickle


def preprocess_data(x_test, scaler_x, standarizer):
    if x_test.ndim == 1:
    	x_test = x_test.reshape([1, x_test.shape[0]])
    x_test_scaled = scaler_x.transform(x_test)
    x_test_preproc = standarizer.transform(x_test_scaled)
    
    return x_test_preproc


def load_y_scaler(path):
    with open(path, "rb") as f:
        y_scaler = pickle.load(f)
    return y_scaler

def load_x_scaler(path):
    with open(path, "rb") as f:
        x_scaler = pickle.load(f)
    return x_scaler

def load_standarizer(path):
    with open(path, "rb") as f:
        standarizer = pickle.load(f)
    return standarizer

def dump_transformer(path, transformer):
    with open(path, "wb") as f:
        pickle.dump(transformer, f)
