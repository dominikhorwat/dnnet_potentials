import numpy as np


def print_metrics(metrics):

    mse_params, abs_params, mse_energies, abs_energies = metrics
    print("MSE params: ", mse_params,
          "\nABS params: ", abs_params,
          "\nMSE energies: ", mse_energies,
          "\nABS energies: ", abs_energies)


def brute_force_predict(_x_test, _y_test, x_train, y_train, use="params"):
    if use == "params":
        mse = ((_y_test - y_train)**2).mean(axis=1)
    elif use == "energies":
        mse = ((_x_test - x_train)**2).mean(axis=1)
    else:
        raise ValueError("No such value type, must be 'params' or 'energies'")
    ind = np.where(mse == mse.min())
    return x_train[ind], y_train[ind]


def brute_force_evaluate(_x_test, _y_test, x_train, y_train, use="params"):
    mse_x = []
    mse_y = []
    abs_y = []
    abs_x = []
    for xx, yy in zip(_x_test, _y_test):
        if use == "params":
            mse = (((y_train - yy)**2).sum(axis=1))/y_train.shape[1]
        elif use == "energies":
            mse = (((x_train - xx)**2).sum(axis=1))/x_train.shape[1]
        else:
            raise ValueError(
                "No such value type, must be 'params' or 'energies'")
        # mse = (((y_train - yy)**2).sum(axis=1))/y_train.shape[1]
        ind = np.where(mse == mse.min())
        mse_x.append((((x_train[ind] - xx)**2).sum())/xx.size)
        mse_y.append((((y_train[ind] - yy)**2).sum())/yy.size)
        abs_x.append((np.abs(x_train[ind] - xx)).mean())
        abs_y.append((np.abs(y_train[ind] - yy)).mean())
    mse_params = sum(mse_y)/len(mse_y)
    abs_params = sum(abs_y)/len(abs_y)
    mse_energies = sum(mse_x)/len(mse_x)
    abs_energies = sum(abs_x)/len(abs_x)
    print_metrics([mse_params, abs_params, mse_energies, abs_energies])

    return mse_params, abs_params, mse_energies, abs_energies


def brute_force_evaluate_sample(_x_pred, _y_pred, _x_test, _y_test):
    mse_params = ((_y_pred - _y_test)**2).mean()
    abs_params = (np.abs(_y_pred - _y_test)).mean()
    mse_energies = ((_x_pred - _x_test)**2).mean()
    abs_energies = (np.abs(_x_pred - _x_test)).mean()
    print_metrics([mse_params, abs_params, mse_energies, abs_energies])

    return mse_params, abs_params, mse_energies, abs_energies


def ann_evaluate(_x_test, x_pred, _y_test, y_pred):
    mse_params = ((_y_test - y_pred)**2).mean()
    abs_params = (np.abs(_y_test - y_pred)).mean()
    mse_energies = ((_x_test - x_pred)**2).mean()
    abs_energies = (np.abs(_x_test - x_pred)).mean()
    print_metrics([mse_params, abs_params, mse_energies, abs_energies])

    return mse_params, abs_params, mse_energies, abs_energies
