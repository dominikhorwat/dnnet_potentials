import numpy as np
import json


def scale(data, data_min, data_max, new_min, new_max):
    """ Data linear scaling. """
    dx = new_max - new_min
    if data_max == data_min:
        data_max += 0.000000001
    return ((data - data_min)/(data_max-data_min))*dx + new_min


def scale_data(y, parameters, new_min, new_max):
    """ Linear data scale. """
    y_scaled = y.copy()
    for n, params in enumerate(parameters):
        data_min, data_max = params[1]
        y_scaled[:, n] = scale(y_scaled[:, n], data_min, data_max,
                               new_min, new_max)

    return y_scaled


def scale_predicted_data(y_pred, parameters, data_min=0, data_max=1):
    """ Revers linear data scale. """
    y_pred_scaled = y_pred.copy()
    for n, params in enumerate(parameters):
        new_min, new_max = params[1]
        y_pred_scaled[:, n] = scale(y_pred_scaled[:, n], data_min, data_max,
                                    new_min, new_max)

    return y_pred_scaled


def transform_input_data(input):
    """ Input data transformations. """
    input_transf = input.copy()
    # mean shifting
    mean = np.mean(input, axis=0).reshape(1, input.shape[1])
    input_transf -= mean
    # multiplying by standard deviation
    std = np.std(input, axis=0).reshape(1, input.shape[1])
    input_transf = np.multiply(input_transf, std)

    return input_transf


def make_prediction(x_test, model, parameters, data_min=0, data_max=1):
    """ Make pradiction for test data. """
    if x_test.ndim == 1:
        x_test = x_test.reshape(1, x_test.shape[0])
    y_pred = model.predict_on_batch(x_test)

    return scale_predicted_data(y_pred, parameters, data_min, data_max)


def validate_prediction(y_test, y_pred):
    """ Validate predicted vs true data by calculating abs difference, mean
        difference, standard deviation of differences and minimu and maximum
        difference.
    """
    abs_difference = np.abs(np.array(y_pred) - np.array(y_test))
    mean_difference = abs_difference.mean(axis=0)
    std_difference = abs_difference.std(axis=0)
    _min = abs_difference.min(axis=0)
    _max = abs_difference.max(axis=0)

    return abs_difference, mean_difference, std_difference, _min, _max


def _format(x):
    """ Format numbers to display the specific decimal numbers. """
    return ("{0:.2f}".format(x) if x >= 100 else "{0:.3f}".format(x)
            if x >= 10 else "{0:.4f}".format(x))


def best_worst_prediction(y_test, y_pred, abs_difference, func):
    """ Find the best or the worst prediction. """
    n = abs_difference.shape[1]
    mse = ((abs_difference**2).sum(axis=1)/n)
    mse_stat = np.apply_over_axes(func, mse, [0])[0]
    min_index = np.where(mse == mse_stat)[0]
    best_y_pred = y_pred[min_index][0]
    best_y_test = y_test[min_index][0]
    best_diff = abs_difference[min_index][0]

    return best_y_pred, best_y_test, best_diff, mse_stat


def show_best_worst_prediction(best_y_test, best_y_pred, best_diff, mse,
                               l2_norm, potential_type, type):
    """ Show the best or the worst prediction. """
    _best_y_test = best_y_test.tolist()
    _best_y_pred = best_y_pred.tolist()
    _best_diff = best_diff.tolist()
    if type == "best":
        print("Best prediction:")
    elif type == "worst":
        print("Worst prediction:")
    else:
        print("The type of prediction is not known")
        raise Exception
    if potential_type == "delr":
        print("Params:\t\t|Radius\t|Depth\t|Beta\t|Beta1\t|CMM1\t|"
              " CMM2\t|CMM3")
        print("--------------------------------------------------------------")
        print("\t\t|\t|\t|\t|\t|\t|\t|\t")
        print("Expected:\t|" + "\t|".join(list(map(_format, _best_y_test))))
        print("\t\t|\t|\t|\t|\t|\t|\t|\t")
        print("Predicted:\t|" + "\t|".join(list(map(_format, _best_y_pred))))
        print("\t\t|\t|\t|\t|\t|\t|\t|\t")
        print("Difference:\t|" + "\t|".join(list(map(_format, _best_diff))))
    elif potential_type == "emo3":
        print("Params:\t\t|Radius\t\t|Depth\t\t|Beta\t\t|Beta1\t\t|Beta2\t\t|"
              " Beta3")
        print("--------------------------------------------------------------")
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Expected:\t|" + "\t\t|".join(list(map(_format, _best_y_test))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Predicted:\t|" + "\t\t|".join(list(map(_format, _best_y_pred))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Difference:\t|" + "\t\t|".join(list(map(_format, _best_diff))))
    elif potential_type == "emo6":
        print("Params:\t\t|Radius\t\t|Depth\t\t|Beta\t\t|Beta1\t\t|Beta2\t\t|"
              " Beta3\t\t|Beta4\t\t|\t\t|Beta5\t\t|\t\t|Beta6\t\t|")
        print("--------------------------------------------------------------")
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Expected:\t|" + "\t\t|".join(list(map(_format, _best_y_test))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Predicted:\t|" + "\t\t|".join(list(map(_format, _best_y_pred))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Difference:\t|" + "\t\t|".join(list(map(_format, _best_diff))))
    else:
        raise Exception
    print("")
    print("MSE:\t\t|{0:.6}".format(mse))
    print("L2 norm:\t\t|{0:.6}".format(l2_norm))


def compare_prediction_and_test(mean_difference, std_difference, _min, _max,
                                parameters):
    """ Compare predicted and true data. """
    print("Validate prediction\n")
    print("Param\t|Mean\t\t|STD\t\t|Min\t\t|Max")
    print("------------------------------------------------------------------")
    for n, params in enumerate(parameters):
        param_name = params[0]
        print(param_name + "\t|{0:.8}\t|{1:.8}\t|{2:.8}\t|{3:.8}".format(
            mean_difference[n], std_difference[n],
            _min[n], _max[n]))
        print("\t|\t\t|\t\t|\t\t")


def show_example_prediction(y_test, y_pred, potential_type):
    """ Show prediction vs true data for one test sample. """
    if potential_type == "delr":
        print("Params:\t\t|Radius\t\t|Depth\t\t|Beta1\t\t|Beta2\t\t|CMM1\t\t|"
              " CMM2\t\t|CMM3")
        print("--------------------------------------------------------------")
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Expected:\t|" + "\t\t|".join(list(map(_format, y_test))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Predicted:\t|" + "\t\t|".join(list(map(_format, y_pred))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Difference:\t|" + "\t\t|".join(
            list(map(_format, np.abs(y_test - y_pred)))))
    elif potential_type == "emo3":
        print("Params:\t\t|Radius\t\t|Depth\t\t|Beta\t\t|Beta1\t\t|Beta2\t\t|"
              " Beta3")
        print("--------------------------------------------------------------")
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Expected:\t|" + "\t\t|".join(list(map(_format, y_test))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Predicted:\t|" + "\t\t|".join(list(map(_format, y_pred))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Difference:\t|" + "\t\t|".join(
            list(map(_format, np.abs(y_test - y_pred)))))
    elif potential_type == "emo6":
        print("Params:\t\t|Radius\t\t|Depth\t\t|Beta\t\t|Beta1\t\t|Beta2\t\t|"
              " Beta3\t\t|Beta4\t\t|\t\t|Beta5\t\t|\t\t|Beta6\t\t|")
        print("--------------------------------------------------------------")
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Expected:\t|" + "\t\t|".join(list(map(_format, y_pred))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Predicted:\t|" + "\t\t|".join(list(map(_format, y_pred))))
        print("\t\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
        print("Difference:\t|" + "\t\t|".join(
            list(map(_format, np.abs(y_test - y_pred)))))
    else:
        raise Exception


def show_metrics(history_train, history_test,
                 metrics=['mean_squared_error', 'mean_absolute_error']):
    """ Show ANN train and validation metrics. """
    best_index = min(history_train.history[metrics[0]])
    best_index = history_train.history[metrics[0]].index(best_index)
    metrics = {
        "Metrics training": {
            "loss": str(history_train.history['loss'][best_index]),
            metrics[0]: str(history_train.history[metrics[0]][best_index]),
            metrics[1]: str(
                history_train.history[metrics[1]][best_index]),
            "val_loss": str(history_train.history['val_loss'][best_index]),
            "val_" + metrics[0]: str(
                history_train.history['val_'+metrics[0]][best_index]),
            "val_"+metrics[1]: str(
                history_train.history['val_'+metrics[1]][best_index])},
        "Metrics test": {
            "loss": str(history_test[0]),
            metrics[0]: str(history_test[1]),
            metrics[1]: str(history_test[2])}}
    print(json.dumps(metrics, indent=4))

    return metrics
