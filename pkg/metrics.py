import numpy as np
from pkg.potentials import compute_emo_potential, compute_delr_potential


def integrate(pot, h):
    """ Calculate integral of given function using the trapezoid rule. """
    p2 = pot[1:]
    p1 = pot[:-1]

    return (h*(p1+p2)/2).sum()


def l2_norm(y_test, y_pred, potential_fcn, radii, e):
    """ Calculate the L2 norm (continuous) for given functions. """
    pot_pred = potential_fcn(radii, y_pred)
    pot_true = potential_fcn(radii, y_test)
    if (pot_pred.size == 0) or (pot_true.size == 0):
        return np.inf

    return np.sqrt(integrate(np.power(pot_pred - pot_true, 2), e))


def compute_l2_norm(radii, y_true, y_pred, potential_type, e=0.01):
    """ Calculate the L2 norm metrics. """
    if y_true.ndim == 1:
        y_true = y_true.reshape(1, y_true.shape[0])
    if y_pred.ndim == 1:
        y_pred = y_pred.reshape(1, y_pred.shape[0])
    zipped = list(map(lambda x: (np.array(x[0]), np.array(x[1]), radii),
                  zip(y_pred.tolist(), y_true.tolist())))

    if potential_type.startswith("emo"):
        return np.mean(list(map(lambda x: l2_norm(x[0], x[1],
                                                  compute_emo_potential, radii,
                                                  e), zipped)))
    elif potential_type.startswith("delr"):
        return np.mean(
            list(map(lambda x: l2_norm(x[0], x[1], compute_delr_potential,
                                       radii, e), zipped)))
    else:
        raise Exception
