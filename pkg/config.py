# Parameters of analytical form of EMO3 potential of CdAr
EMO3_CdAr_PARAMETERS = [("R", (3.48, 3.54)),
                        ("Depth", (300, 330)),
                        ("Beta", (1.3, 1.6)),
                        ("Beta2", (0.0, 0.8)),
                        ("Beta3", (0.0, 0.6)),
                        ("Beta4", (0.0, 0.6))]

# Parameters of analytical form of DELR potential  of CdAr
DELR_CdAr_PARAMETERS = [("R", (3.48, 3.54)),
                        ("Depth", (300, 330)),
                        ("Beta", (1.3, 1.6)),
                        ("Beta2", (0.0, 0.8)),
                        ("Beta3", (0.0, 0.6)),
                        ("CMM1", (-1.0, 0.0)),
                        ("CMM2", (0.0, 1.0)),
                        ("CMM3", (0.0, 0.6))]

# Parameters of analytical form of EMO3 potential of MgF
EMO3_MgF_PARAMETERS = [("R", (1.7, 1.8)),
                       ("Depth", (37297.24, 37297.24)),
                       ("Beta", (1.1, 1.8)),
                       ("Beta2", (-0.5, 0.5)),
                       ("Beta3", (-0.3, 0.3)),
                       ("Beta4", (-0.3, 0.3))]

# Parameters of analytical form of EMO3 potential of MgF
EMO3_NOT_FIXED_MgF_PARAMETERS = [("R", (1.7, 1.8)),
                                 ("Depth", (37280, 37320)),
                                 ("Beta", (1.1, 1.8)),
                                 ("Beta2", (-0.5, 0.5)),
                                 ("Beta3", (-0.3, 0.3)),
                                 ("Beta4", (-0.3, 0.3))]

# Parameters of analytical form of EMO4 potential of MgF
EMO4_MgF_PARAMETERS = [("R", (1.7, 1.8)),
                       ("Depth", (37297.24, 37297.24)),
                       ("Beta", (1.1, 1.8)),
                       ("Beta2", (-0.5, 0.5)),
                       ("Beta3", (-0.3, 0.3)),
                       ("Beta4", (-0.3, 0.3)),
                       ("Beta5", (-0.2, 0.2))]

# Parameters of analytical form of EMO5 potential of MgF
EMO5_MgF_PARAMETERS = [("R", (1.7, 1.8)),
                       ("Depth", (37297.24, 37297.24)),
                       ("Beta", (1.1, 1.8)),
                       ("Beta2", (-0.5, 0.5)),
                       ("Beta3", (-0.3, 0.3)),
                       ("Beta4", (-0.3, 0.3)),
                       ("Beta5", (-0.2, 0.2)),
                       ("Beta6", (-0.2, 0.2))]

# Parameters of analytical form of EMO6 potential of MgF
EMO6_MgF_PARAMETERS = [("R", (1.7, 1.8)),
                       ("Depth", (37297.24, 37297.24)),
                       ("Beta", (1.1, 1.8)),
                       ("Beta2", (-0.5, 0.5)),
                       ("Beta3", (-0.3, 0.3)),
                       ("Beta4", (-0.3, 0.3)),
                       ("Beta5", (-0.2, 0.2)),
                       ("Beta6", (-0.2, 0.2)),
                       ("Beta7", (-0.2, 0.2))]
