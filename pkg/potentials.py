import numpy as np


def compute_emo_potential(r, params):
    """ Compute Extended Morse Oscillator (EMO) potential. """
    radius = params[0]
    depth = params[1]
    betas = params[2:]
    index = 0
    for power, beta in enumerate(betas):
        index += -beta*(((r-radius)/(r+radius))**(power))*(r-radius)
    potential = depth*((1-np.exp(index))**2)

    return potential


def compute_delr_potential(r, params):
    """ Compute Double Exponential Long-Range (DELR) potential. """
    radius = params[0]
    depth = params[1]
    betas = params[2:]
    index = 0
    for power, beta in enumerate(betas):
        index += -beta*((r-radius))**(power+1)
    a = 0
    b = 0
    ulr = 0
    potential = a*np.exp(2*index)-b*np.exp(index) + ulr

    return np.array([])


def get_potential_energy_curve(radii, params, potential_type):
    """ Get potential energy curve. """
    if potential_type.startswith("emo"):
        return [compute_emo_potential(r, params) for r in radii]
    elif potential_type.startswith("delr"):
        return [compute_delr_potential(r, params) for r in radii]
    else:
        raise ValueError("No such type of potential!")
