import keras.backend as K
# import tensorflow as tf
import numpy as np
from pkg.data_utils import scale
from pkg.config import EMO3_MgF_PARAMETERS


def compute_emo_potential(r, data):
    """ Compute Extended Morse Oscillator (EMO) potential. """
    radius = data[0]
    depth = data[1]
    betas = data[2:]

    r_min, r_max = EMO3_MgF_PARAMETERS[0][1]
    depth_min, depth_max = EMO3_MgF_PARAMETERS[1][1]

    radius = scale(radius, 0, 1, r_min, r_max)
    depth = scale(depth, 0, 1, depth_min, depth_max)
    betas_scaled = [scale(betas[n], 0, 1, params[1][0], params[1][1])
                    for n, params in enumerate(EMO3_MgF_PARAMETERS[2:])]

    # b1 = scale(betas[0], 0, 1, 1.3, 1.6)
    # b2 = scale(betas[1], 0, 1, 0, 0.8)
    # b3 = scale(betas[2], 0, 1, 0, 0.6)
    # b4 = scale(betas[3], 0, 1, 0, 0.6)
    # b = [b1, b2, b3, b4]
    index = 0
    for n in range(betas.shape[0]):
        index += -betas_scaled[n]*((r-radius))**(n+1)
    potential = depth*((1-K.exp(index))**2)

    return potential


def integrate(function, h):
    """ Calculate integral of given function using the trapezoid rule. """
    p2 = tf.identity(function)[1:]
    p1 = function[:-1]

    return K.sum(h*(p1+p2)/2)


def l2_norm(y_pred, y_true):
    """ Calculate the L2 norm (continuous) for given functions. """
    h = 0.05
    r = np.arange(1.25, 8, h)
    pot_pred = compute_emo_potential(r, y_pred)
    pot_true = compute_emo_potential(r, y_true)

    return K.sqrt(integrate(K.pow(pot_pred - pot_true, 2), h))


def lp_norm_loss(y_true, y_pred):
    """ L2 norm loss function. """
    return K.map_fn(
        lambda x: l2_norm(x[0], x[1]), K.stack([y_pred, y_true], axis=1))


def weighted_mse(weights):
    def mse_loss(y_true, y_pred):
        if not K.is_tensor(y_pred):
            y_pred = K.constant(y_pred)
        y_true = K.cast(y_true, y_pred.dtype)
        return K.mean(K.square(y_pred - y_true)*K.constant(weights), axis=-1)
    return mse_loss
