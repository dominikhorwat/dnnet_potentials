from matplotlib import pyplot as plt
from pkg.potentials import get_potential_energy_curve


def plot_potential(radii,
                   potential_ref,
                   potential_pred,
                   cut_point=0,
                   fontsize=10):
    """ Plot potential: reference, predicted and reference vs predicted. """
    ymax = max(potential_pred[cut_point:]) + 100
    plt.figure(figsize=(18, 8))
    plt.subplot(1, 3, 1)
    plt.title('Reference')
    plt.plot(radii[cut_point:], potential_ref[cut_point:])
    plt.xlabel(r"R $[Å]$",  fontsize=fontsize)
    plt.ylabel(r"Energy [$cm^-1$]",  fontsize=fontsize)
    plt.ylim([-10, ymax])
    plt.tick_params(labelsize=fontsize)
    plt.subplot(1, 3, 2)
    plt.title('Predicted')
    plt.plot(radii[cut_point:], potential_pred[cut_point:])
    plt.xlabel(r"R $[Å]$",  fontsize=fontsize)
    plt.ylabel(r"Energy [$cm^-1$]",  fontsize=fontsize)
    plt.tick_params(labelsize=fontsize)
    plt.subplot(1, 3, 3)
    plt.title('Reference vs Predicted')
    plt.plot(radii[cut_point:], potential_pred[cut_point:], 'ob',
             label="Predicted")
    plt.plot(radii[cut_point:], potential_ref[cut_point:], '-r',
             label="Reference")
    plt.xlabel(r"R $[Å]$",  fontsize=fontsize)
    plt.ylabel(r"Energy [$cm^-1$]",  fontsize=fontsize)
    plt.ylim([-10, ymax])
    plt.tick_params(labelsize=fontsize)
    plt.legend()
    plt.show()
    

def plot_single_potential(radii, y_pred, cut_point=0, fontsize=10):
    plt.figure(figsize=(8, 8))
    potentials = get_potential_energy_curve(radii, y_pred, "emo3")
    plt.title('Predicted')
    plt.plot(radii[cut_point:], potentials[cut_point:])
    plt.xlabel(r"R $[Å]$",  fontsize=fontsize)
    plt.ylabel(r"Energy [$cm^-1$]",  fontsize=fontsize)
    plt.tick_params(labelsize=fontsize)
    plt.grid()
    plt.show()


def plot_model_training(history,
                        metrics=['mean_squared_error', 'mean_absolute_error']):
    """ Plot model training history. """
    plt.figure(figsize=(12, 6))
    plt.plot(history.epoch, history.history["val_"+metrics[0]], "r--")
    plt.plot(history.epoch, history.history["val_"+metrics[1]], "k--")
    plt.plot(history.epoch, history.history[metrics[0]])
    plt.plot(history.epoch, history.history[metrics[1]])
    plt.title('ANN training')
    plt.xlabel("Epochs")
    plt.ylabel("Error")
    plt.legend(["val_"+metrics[0], "val_"+metrics[1], metrics[0], metrics[1]])
    plt.grid()


def plot_histograms(abs_difference, parameters):
    """ Plot histograms of differences between predicted and true potential
        parameters.
    """
    plt.figure(figsize=(18, 9))
    lines = int(len(parameters)/2)
    for n, params in enumerate(parameters):
        param_name = params[0]
        values = params[1]
        plt.subplot(2, lines, (n+1))
        plt.title(param_name + ": " + str(values))
        plt.hist(abs_difference[:, n], bins=20, rwidth=0.9)
    plt.show()
