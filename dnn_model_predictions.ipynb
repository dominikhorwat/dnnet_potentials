{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  Determine EMO parameters for selected molecule\n",
    "------------------\n",
    "\n",
    "## Methodology\n",
    "\n",
    "A method which employs a deep neural network (DNN) for obtaining parameters of analytical potential energy curve (PEC) of the selected electronic state of a diatomic molecule based on the energies of recorded ro-vibrational transitions. PEC is in form of expanded Morse oscillator (EMO) function with 6 parameters (EMO3), i.e:\n",
    "- De (the potential well depth),\n",
    "- Re (the internuclear equilibrium distance),\n",
    "- β0, β1, β2, β3.\n",
    "\n",
    "\n",
    "## General information\n",
    "You can determine parameters of the EMO3 potential (6 parameters) of the ground state in a given molecule. By default, this script determines potential parameters of EMO3 potential of the ground state in MgF molecule using our model (``model_mgf.h5``) and test data we supply. You can also determine parameters for MgF for your own data. You can even determine parameters for other dimer molecule(s) using your own model, trained for molecule you selected. The only thing you need to do is to provide the appropriate path to your data and/or model.\n",
    "\n",
    "\n",
    "## Paths\n",
    "Depending on what data/model you are using, you should change the following paths accordingly (**Define paths** section):\n",
    "1) data provided by authors for the ground state in MgF\n",
    "You should stay with default sets.\n",
    "\n",
    "2) user data and model for other dimer\n",
    "- data paths\n",
    "    - path to your test data: path_test_data (``./data/mgf/artificial/test/`` by default)\n",
    "    - filename of your input (energies) data: test_input_name (``inp.csv`` by default)\n",
    "\n",
    "- config paths\n",
    "    - path to your standarizer: path_standarizer (``./configs/standarizer.pk`` by default)\n",
    "    - path to your scaler for input data: path_scaler_x (``./configs/scaler_x.pk``)\n",
    "    - path to your scaler for output data: path_scaler_y (``./configs/scaler_y.pk`` by default)\n",
    "\n",
    "- model paths\n",
    "    - path to your model: path_model (``./models/model_mgf.h5`` by default)\n",
    "    - path to your hyperparams used during your model training: path_hyperparams (``./models/model_mgf_hyperparams.json`` by default)\n",
    "\n",
    "\n",
    "## Data preparation\n",
    "\n",
    "### Input\n",
    "\n",
    "To determine the parameters of EMO3 potential of the ground state for a given molecule, you must prepare only input data formed by the energies of ro-vibrational transitions of the ground state in this molecule. Data must be placed in csv file. You can determine parameters for a single instance (one set of transition energies) or multiple different instances (multiple different sets of transition energies). In later, each instance must be placed in the separate row. A single instance must contain 744 energy of assigned transitions between different ro-vibrational levels (see paper for more details).\n",
    "\n",
    "\n",
    "### Example\n",
    "\n",
    "In ``data`` dir you can find data for MgF molecule. Dataset consists sample of 1000 instances of transitions energies and potential parameters splited into train and test subsets (900 and 100, respectively). This data can be directly use to play with the trained model or can be used to train your own model (this number of data is inssuficient to train the new model, to get the full dataset please contact authors). If you want to train your own model, dataset should have the same structure as example data, i.e.\n",
    "\n",
    "- potential parameters\n",
    "```\n",
    "    1.7661;37297.24;1.2570;0.4290;-0.1865;-0.1932\n",
    "    1.7473;37297.24;1.4954;0.1950;-0.0239;0.0925\n",
    "\n",
    "                      ...\n",
    "                      ...\n",
    "                      ...\n",
    "\n",
    "    1.7775;37297.24;1.4606;-0.1120;-0.0285;-0.1298\n",
    "```\n",
    "where each column stores value for Re, De, β0, β1, β2, β3 parameter, respectively.\n",
    "\n",
    "\n",
    "- transition energies\n",
    "```\n",
    "    608.3629;610.3959;611.4110;612.4250;613.4369;...;523.7549\n",
    "    723.2280;725.2779;726.2949;727.3060;728.3119;...;625.4170\n",
    "\n",
    "                          ...\n",
    "                          ...\n",
    "                          ...\n",
    "\n",
    "    704.6990;706.6650;707.6359;708.5990;709.5539;...;597.8220\n",
    "```\n",
    "where each column stores energy of assigned transitions between different ro-vibrational levels (see paper for more details).\n",
    "\n",
    "**Please refer to ``doc.txt`` to get full documentation.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--------\n",
    "--------\n",
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import libs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "warnings.simplefilter(action='ignore', category=FutureWarning)\n",
    "import numpy as np\n",
    "np.set_printoptions(suppress=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import time\n",
    "import json\n",
    "import numpy as np\n",
    "import keras.backend as K\n",
    "from keras.models import load_model\n",
    "from sklearn.preprocessing import MinMaxScaler, StandardScaler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pkg.read_data import read_test_input_from_csv\n",
    "from pkg.keras_metrics import weighted_mse\n",
    "from pkg.plots import plot_single_potential\n",
    "from pkg.model import load_deep_model, make_predictions, show_predictions\n",
    "from pkg.preprocessing import load_y_scaler, load_x_scaler, load_standarizer, preprocess_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--------\n",
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define paths"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_test_data = \"./data/mgf/artificial/test/\"\n",
    "test_input_name = \"inp.csv\"\n",
    "test_output_name = \"out.csv\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_standarizer = \"./configs/standarizer.pk\"\n",
    "path_scaler_x = \"./configs/scaler_x.pk\"\n",
    "path_scaler_y = \"./configs/scaler_y.pk\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_model = \"./models/model_mgf.h5\"\n",
    "path_hyperparams = \"./models/model_mgf_hyperparams.json\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--------\n",
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load hyperparams"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(path_hyperparams, 'r') as f:\n",
    "    hyperparams = json.load(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load DNN model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "model = load_deep_model(path_model, hyperparams, weighted_mse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_test = read_test_input_from_csv(path_test_data, test_input_name, delimiter=\";\")[:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocess data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Scale data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transform input data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler_x = load_x_scaler(path_scaler_x)\n",
    "scaler_y = load_y_scaler(path_scaler_y)\n",
    "\n",
    "standarizer = load_standarizer(path_standarizer)\n",
    "x_test_preproc = preprocess_data(x_test, scaler_x, standarizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prediction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred = make_predictions(x_test_preproc, scaler_y, model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show prediction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show_predictions(y_pred)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot PEC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "radii = list(np.arange(1.12,8,0.01))\n",
    "for params in y_pred:\n",
    "    plot_single_potential(radii, params)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
